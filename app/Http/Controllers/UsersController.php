<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UsersResource;



class UsersController extends Controller
{
    public function index () {
        $user = User::All();

        return UsersResource::collection($user);
    }

    public function store (Request $request) {
        $user = User::create([
            'name' => $request->nama
        ]);

        return $user;
    }

    public function update ($id, Request $request) {
        $user_id =User::where('id', $id)->first();
        $user = $user_id->update([
            'name' => $request->nama
        ]);

        return $user_id;
    }

    public function delete ($id) {
        User::destroy($id);

        return 'success';
    }
}
