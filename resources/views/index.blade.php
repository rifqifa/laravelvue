<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div id="app">
        <input type="text" v-model="nama">
        <button id=AddEdit v-on:click="AddorUpdate">Add</button>
        <ul>
            <li v-for="(user, key) in users">
                    @{{user.nama}}
                    <button v-on:click="ubah(key, user)"> edit </button>
                    <button v-on:click="hapus(key, user)"> delete </button>
            </li>
        </ul>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"> </script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    <script>
    var vo = new Vue({
    el : '#app',
    data : {
        users: [],
        nama: "",
        indexuser:0,
        userid:0,
    },
    methods : {
        AddorUpdate: function () {
            if (document.querySelector('#AddEdit').textContent == 'Add') {
                this.$http.post('/api/user',{nama:this.nama}).then(response => {
                    this.users.push({nama: this.nama});
                    this.nama=''
                });
            } else {
                this.$http.post('/api/user/update/'+this.userid,{nama:this.nama}).then(response => {
                    this.users[this.indexuser].nama = this.nama;
                    document.querySelector('#AddEdit').textContent = 'Add';
                    this.nama=''
                })
            }
            
        },
        ubah: function (key, user) {
            this.nama = this.users[key].nama;
            document.querySelector('#AddEdit').textContent = 'edit';
            this.indexuser = key;
            this.userid = user.id;
        },
        hapus: function (key, user) {
            this.userid = user.id;
            var hapusdata = confirm("hapus data?");
            if (hapusdata) {
            console.log(this.userid)
                this.$http.post('/api/user/delete/'+this.userid).then(response => {
                    this.users.splice(key,1);
                })
            }
        },
    },
    mounted : function () {
        
        this.$http.get('/api/user').then(response => {
            let result = response.body.data
            this.users = result
        });

    },
    watch: {
        nama: function (val) {
            this.nama = val
        }
    }
}) 
    </script>
</body>
</html>